/*
Authors
* Sebastien 'sinux' Chassot <seba.ptl@sinux.net>
* Alexandre Rosenberg
* Maxime Borges <contatc@maximeborg.es>
* Martin Voelkle <martin.voelkle@gmail.com>
*/

// Uncomment to switch to debug mode; this will allow to move the galvos and see corresponding values to align them properly using the maps
// #define DEBUG

#include <SPI.h>
#include <Bounce.h>

#include <Dhcp.h>
#include <EthernetClient.h>
#include <Dns.h>
#include <Ethernet.h>

// See `config.example.h` for an example of config
#include "config.h"

namespace StreamFile {
  static Stream& get_stream(FILE* file) {
    return *static_cast<Stream*>(fdev_get_udata(file));
  }

  int put(char c, FILE* file) {
    get_stream(file).print(c);
    return 0;
  }

  int get(FILE* file) {
    auto&& byte = get_stream(file).read();
    if (byte == -1) return _FDEV_EOF;
    return byte;
  }

  FILE* open(Stream& stream) {
    auto&& file = fdevopen(&put, &get);
    fdev_set_udata(file, &stream);
    return file;
  }
}
auto&& serial_file = StreamFile::open(Serial);

template<size_t capacity>
struct PrintfString {
  char data[capacity];
  size_t size;

  PrintfString(): size{0} {
  }

  template<typename Format, typename... Arguments>
  void printf(Format format, Arguments... arguments) {
    if (size == capacity) {
      return;
    }

    auto&& remaining = capacity - size;
    auto&& printed = snprintf(data + size, remaining, format, arguments...);
    if (printed < 0) {
      Serial.println("Print error!");
      return;
    }

    auto&& printed_size = size_t(printed);
    if (printed_size < remaining) {
      size += printed_size;
      return;
    }

    Serial.println("Buffer overflow!");
    data[capacity - 1] = '\0';
    size = capacity;
  }
};

byte mac_address[] = { 0x90, 0xA2, 0xDA, 0x00, 0x2C, 0x86 };
EthernetClient client;
auto&& client_file = StreamFile::open(client);

// Pins
const uint8_t PIN_LED_POWER = A0;
const uint8_t PIN_LED_R = A1;
const uint8_t PIN_LED_G = A2;
const uint8_t PIN_LED_B = A3;

const uint8_t PIN_PWR_SWITCH = 7;

const uint8_t PIN_LEV_L_L = 8;
const uint8_t PIN_LEV_L_R = 9;
const uint8_t PIN_LEV_R_L = 3;
const uint8_t PIN_LEV_R_R = 2;

const uint8_t PIN_GALV_L = 6;
const uint8_t PIN_GALV_R = 5;

// Const
const uint32_t QUARTER_HOUR_IN_MS = 15 * 60 * 1000ul;
// Period of power LED blink when the lab is not open
const uint32_t BLINK_PERIOD_MS = 2000;
// Duration of the `on` period for lab closed blink
const uint32_t BLINK_ON_DURATION_MS = 500;
// Fast blink period when the lab is about to close
const uint32_t ALMOST_CLOSED_BLINK_PERIOD_MS = 500;

// In quarter of hours
const uint8_t T_QUARTERS_LEFT_SUBDIV = 4;
const uint8_t T_QUARTERS_LEFT_MAX = 16 * T_QUARTERS_LEFT_SUBDIV;
const uint8_t PEOPLE_MAX = 14;

const uint32_t LEVER_TIME_LEFT_INTERVAL_MS = 100;
const uint32_t LEVER_PEOPLE_INTERVAL_MS = 300;

// Macros
#define LED_POWER_On (digitalWrite(PIN_LED_POWER, HIGH))
#define LED_POWER_Off (digitalWrite(PIN_LED_POWER, LOW))
#define LED_R_On (digitalWrite(PIN_LED_R, LOW))
#define LED_R_Off (digitalWrite(PIN_LED_R, HIGH))
#define LED_B_On (digitalWrite(PIN_LED_B, LOW))
#define LED_B_Off (digitalWrite(PIN_LED_B, HIGH))
#define LED_G_On (digitalWrite(PIN_LED_G, LOW))
#define LED_G_Off (digitalWrite(PIN_LED_G, HIGH))

// Levers and switch are using the debounce library `Bounce`
// We are using 20ms of debounce time
Bounce pwr_switch = Bounce(PIN_PWR_SWITCH, 20);
Bounce lev_r_r = Bounce(PIN_LEV_R_R, 20);
Bounce lev_r_l = Bounce(PIN_LEV_R_L, 20);
Bounce lev_l_r = Bounce(PIN_LEV_L_R, 20);
Bounce lev_l_l = Bounce(PIN_LEV_L_L, 20);

// Time of the last change of the levers
uint32_t last_lever_left_change_ms = 0;
uint32_t last_lever_right_change_ms = 0;

// Time left in quarter of hours
uint8_t t_quarters_left = 0;
uint8_t n_person = 0;

// Map for the time left lever to make it properly aligned with the graphics
const uint8_t t_quarters_left_map[] = {
  0,
  26,
  50,
  63,
  76,
  89,
  102,
  117,
  129,
  143,
  159,
  173,
  189,
  204,
  219,
  237,
  254,
};

// Map for the person lever to make it properly aligned with the graphics
const uint8_t n_person_map[] = {
  0,
  24,
  54,
  69,
  85,
  102,
  119,
  135,
  151,
  170,
  187,
  205,
  222,
  235,
  251,
};

// Reference time to decrease the time left
uint32_t reftime_ms = 0;
// Last time we blinked the power LED
uint32_t last_led_blink_ms = 0;

// Used to send update when there is change using the levers
// Reset to 0 once the update is sent
uint32_t last_change_ms = 0;

void updateStatus() {
  auto&& t_minutes_left = t_quarters_left * 15;

  auto&& status = PrintfString<128>{};
  if (t_minutes_left == 0) {
    status.printf("The lab is closed.");
  } else if (t_minutes_left > 0 and t_minutes_left < 60) {
    status.printf("The lab is open - remaining time is %u min.", t_minutes_left);
  } else {
    status.printf("The lab is open - planned to be open for %uh%u.", t_minutes_left / 60, t_minutes_left % 60);
  }
  status.printf(" ");
  if (n_person == 0) {
    status.printf("Nobody here !");
  } else if (n_person == 1) {
    status.printf("One lonely hacker in the space.");
  } else {
    status.printf("There are %u hackers in the space.", n_person);
  }
  status.printf("  [Set by PTL control panel]");

  auto&& request_body = PrintfString<256>{};
  request_body.printf(
    "api_key=%s&status=%s&open_closed=%s&people_now_present=%u&submit=%s",
    API_KEY,
    status.data,
    t_quarters_left ? "open" : "closed",
    int{n_person},
    "Submit"
  );

  Serial.println(request_body.data);
  if (!client.connect(SERVER_HOSTNAME, SERVER_PORT)) {
    Serial.println("connection failed");
    return;
  }

  Serial.println("connected - status update");
  fprintf(client_file, "POST /change_status HTTP/1.1\n");
  fprintf(client_file, "Host: %s\n", SERVER_HOSTNAME);
  fprintf(client_file, "User-Agent: ControlPanel/2.0\n");
  fprintf(client_file, "Content-Length: %u\n", request_body.size);
  fprintf(client_file, "Content-Type: application/x-www-form-urlencoded\n");
  fprintf(client_file, "\n%s\n", request_body.data);
  Serial.println("disconnecting.");
  client.stop();
}


void update_buttons_debounce() {
  pwr_switch.update();
  lev_r_r.update();
  lev_r_l.update();
  lev_l_r.update();
  lev_l_l.update();
}


// Main functions
void setup() {
  pinMode(PIN_PWR_SWITCH, INPUT_PULLUP);
  pinMode(PIN_LEV_L_L, INPUT_PULLUP);
  pinMode(PIN_LEV_L_R, INPUT_PULLUP);
  pinMode(PIN_LEV_R_L, INPUT_PULLUP);
  pinMode(PIN_LEV_R_R, INPUT_PULLUP);

  pinMode(PIN_LED_POWER, OUTPUT);
  pinMode(PIN_LED_R, OUTPUT);
  pinMode(PIN_LED_G, OUTPUT);
  pinMode(PIN_LED_B, OUTPUT);

  LED_POWER_On;
  LED_R_Off;
  LED_G_Off;
  LED_B_On;

  // Open serial communications and wait for port to open:
  // initialize serial:
  Serial.begin(115200);
  Serial.println("Started Serial");

  #ifndef DEBUG

    // [Eth Shield] Start the Network connection
    if (Ethernet.begin(mac_address) == 0) {
      Serial.println("Failed to configure Network DHCP");
      // no point in carrying on, so do nothing forevermore
      while (true);
    }

    // [Eth Shield] Give a second to initialize:
    delay(1000);
    Serial.println("Configured IP using DHCP");
    auto&& local_ip = Ethernet.localIP();
    fprintf(serial_file, "My IP address: %u.%u.%u.%u.\n", local_ip[0], local_ip[1], local_ip[2], local_ip[3]);

    LED_B_Off;
  #endif
}

void loop() {
  update_buttons_debounce();

  #ifndef DEBUG
    // Left lever
    if (millis() - last_lever_left_change_ms > LEVER_TIME_LEFT_INTERVAL_MS) {
      if (!lev_l_r.read()) {
        if (t_quarters_left < T_QUARTERS_LEFT_MAX) {
          t_quarters_left += 1;
        }
        last_lever_left_change_ms = millis();
        last_change_ms = millis();
      } else if (!lev_l_l.read()) {
        if (t_quarters_left > 0) {
          t_quarters_left -= 1;
        }
        last_lever_left_change_ms = millis();
        last_change_ms = millis();
      }
    }

    // Right lever
    if (millis() - last_lever_right_change_ms > LEVER_PEOPLE_INTERVAL_MS) {
      if (!lev_r_r.read()) {
        if (n_person < PEOPLE_MAX) {
          n_person += 1;
        }
        last_lever_right_change_ms = millis();
        last_change_ms = millis();
      } else if (!lev_r_l.read()) {
        if (n_person < PEOPLE_MAX) {
          n_person -= 1;
        }
        last_lever_right_change_ms = millis();
        last_change_ms = millis();
      }
    }

    // Reset the galvos if the main switch is set to off for 2s
    if (pwr_switch.read() && (pwr_switch.duration() > 2000)) {
      t_quarters_left = 0;
      n_person = 0;
    }

    // If space is open, there should be at least one person !
    if ((t_quarters_left > 0) && (n_person == 0)) {
      n_person = 1;
    }

    // If there is no time left, set number of people to 0
    if (t_quarters_left == 0) {
      n_person = 0;
    }

    // Write time left to left galvo
    auto t_quarters_left_map_index = t_quarters_left / T_QUARTERS_LEFT_SUBDIV;
    auto time_mod_index = t_quarters_left % T_QUARTERS_LEFT_SUBDIV;
    auto t_quarters_left_mapped_value = t_quarters_left_map[t_quarters_left_map_index];
    if (time_mod_index != 0) {
      t_quarters_left_mapped_value += (t_quarters_left_map[t_quarters_left_map_index + 1] - t_quarters_left_map[t_quarters_left_map_index]) / T_QUARTERS_LEFT_SUBDIV * time_mod_index;
    }
    analogWrite(PIN_GALV_L, t_quarters_left_mapped_value);

    // Write person to right galvo
    auto n_person_mapped_value = n_person_map[n_person];
    analogWrite(PIN_GALV_R, n_person_mapped_value);

    // Decrement time every 15 minutes
    if ((millis() - reftime_ms) >= QUARTER_HOUR_IN_MS) {
      t_quarters_left -= min(t_quarters_left, 1);
      reftime_ms = millis();
      last_change_ms = millis();
    }


    // Blink the red LED when the lab is closed
    if (t_quarters_left == 0) {
      if ((millis() - last_led_blink_ms) > (BLINK_PERIOD_MS + BLINK_ON_DURATION_MS)) {
        LED_POWER_Off;
        last_led_blink_ms = millis();
      } else if ((millis() - last_led_blink_ms) > BLINK_PERIOD_MS) {
        LED_POWER_On;
      }
      LED_R_Off;
      LED_G_Off;
      LED_B_Off;
    } else {
      // Green if more than 1h30 left
      if (t_quarters_left > 6) {
        LED_R_Off;
        LED_G_On;
        LED_B_Off;
      }
      // Orange if 30min left
      else if (t_quarters_left > 2) {
        LED_R_On;
        LED_G_On;
        LED_B_Off;
      } else {
        // Fast blink red LED when less than 30min left
        static bool almost_closed_led_status = LOW;
        if (millis() - last_led_blink_ms > ALMOST_CLOSED_BLINK_PERIOD_MS) {
          if (almost_closed_led_status == HIGH) {
            LED_R_Off;
            almost_closed_led_status = LOW;
          } else {
            LED_R_On;
            almost_closed_led_status = HIGH;
          }
          LED_G_Off;
          LED_B_Off;
          last_led_blink_ms = millis();
        }
      }
      LED_POWER_Off;
    }

    // Update status when changes stabilized
    if (last_change_ms != 0 && millis() - last_change_ms > 2000) {
      updateStatus();
      last_change_ms = 0;
    }

  #else

    // Debug via serial
    if (Serial.available()) {
      auto s = Serial.readStringUntil('\n');
      if (s.startsWith("t")) {
        t_quarters_left = s.substring(1).toInt();
        Serial.print("new time:");
        Serial.println();

        auto t_quarters_left_map_index = t_quarters_left / T_QUARTERS_LEFT_SUBDIV;
        auto time_mod_index = t_quarters_left % T_QUARTERS_LEFT_SUBDIV;
        auto t_quarters_left_mapped_value = t_quarters_left_map[t_quarters_left_map_index];
        Serial.println(t_quarters_left_map_index);
        Serial.println(time_mod_index);
        Serial.println(t_quarters_left_mapped_value);
        if (time_mod_index != 0) {
          t_quarters_left_mapped_value += (t_quarters_left_map[t_quarters_left_map_index + 1] - t_quarters_left_map[t_quarters_left_map_index]) / T_QUARTERS_LEFT_SUBDIV * time_mod_index;
        }
        Serial.println(t_quarters_left_mapped_value);
        analogWrite(PIN_GALV_L, t_quarters_left_mapped_value);
      } else if (s.startsWith("p")) {
        n_person = s.substring(1).toInt();
        Serial.print("new people:");
        Serial.println(n_person);

        auto n_person_map_index = n_person;
        auto n_person_mapped_value = n_person_map[n_person_map_index];
        Serial.println(n_person_map_index);
        Serial.println(n_person_mapped_value);
        Serial.println(n_person_mapped_value);
        analogWrite(PIN_GALV_R, n_person_mapped_value);
      } else if (s.startsWith("T")) {
        auto time_left_mapped_value = s.substring(1).toInt();
        Serial.print("new time left val:");
        Serial.println(time_left_mapped_value);
        analogWrite(PIN_GALV_L, time_left_mapped_value);
      } else if (s.startsWith("P")) {
        auto n_person_mapped_value = s.substring(1).toInt();
        Serial.print("new people val:");
        Serial.println(n_person_mapped_value);
        analogWrite(PIN_GALV_R, n_person_mapped_value);
      }
      Serial.flush();
    }
  #endif
}
