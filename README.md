# Space API Control Panel
Arduino project for the control panel of the lab status.

# Build

* Prepare the `config.h` file by copying `config.example.h`, and edit the API key.
Ask your favorite sysadmin if you don't currently have access to it.

## arduino-cli

* Install
```
arduino-cli core install arduino:avr
arduino-cli --config-file arduino-cli.yaml lib install --git-url https://github.com/mpflaga/Arduino-Bounce.git
arduino-cli lib install Ethernet
```

* Compile and upload
```
arduino-cli compile --fqbn arduino:avr:uno
arduino-cli upload --fqbn arduino:avr:uno --port /dev/ttyACM0
```

## Arduino IDE 2
### Dependencies
This project is using the `Arduino-Bounce` library which is a bit old, so it is not present in the library manager of Arduino. The easiest solution is to download and install it manually.

- In the IDE, go to File -> Preferences, retrieve the `Sketchbook location` path and navigate there.
- Go into the `libraries` folder
- Download the `Arduino-Bounce` library from https://github.com/mpflaga/Arduino-Bounce.git. Either use git to clone it there, or download the zip version and extract it in this folder. You should now have a `Arduino-Bounce` folder in the `Arduino/libraries/` folder.

### Build
Use the upload button to compile and flash, make sure you selected the right serial port on the top menu.
